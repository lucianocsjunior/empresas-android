package com.example.lucia.testeempresas.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lucia.testeempresas.Objects.Enterprise;
import com.example.lucia.testeempresas.R;

import java.util.ArrayList;

/**
 * Adapter para exibir as listas de empresas
 * @author lucia
 * @version 1.0
 */

public class EnterpriseAdapter extends BaseAdapter {

    private ArrayList<Enterprise> enterprisesList;
    private LayoutInflater layoutInflater;
    private Context context;
    private Enterprise enterprise;
    private ImageView enterpriseImage;
    private TextView enterpriseName,enterpriseBusiness,enterpriseLocation,enterpriseNoLogo;
    private RelativeLayout relativeLayout;
    private String enterpriseNoLogoName;

    public EnterpriseAdapter(ArrayList<Enterprise>enterprisesList, Context context){
        this.context = context;
        this.enterprisesList = enterprisesList;
        this.layoutInflater = (LayoutInflater.from(context));
    }



    @Override
    public int getCount() {
        return enterprisesList.size();
    }

    @Override
    public Object getItem(int position) {
        return enterprisesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.card_layout,null);
         enterprise = enterprisesList.get(position);
         enterpriseImage = (ImageView) convertView.findViewById(R.id.enterprise_logo);
         enterpriseName = (TextView) convertView.findViewById(R.id.enterprise_name);
         enterpriseBusiness = (TextView) convertView.findViewById(R.id.enterpriseBusiness);
         enterpriseLocation = (TextView) convertView.findViewById(R.id.enterpriseLocation);
         relativeLayout = (RelativeLayout) convertView.findViewById(R.id.imageNull);
         enterpriseNoLogo = (TextView)convertView.findViewById(R.id.enterpriseNoLogo);
        if(enterprise.getPhoto() == null){
            enterpriseImage.setVisibility(convertView.INVISIBLE);
            relativeLayout.setBackgroundColor(R.color.buttonColor);
            enterpriseNoLogoName = "E"+enterprise.getId();
            enterpriseNoLogo.setText(enterpriseNoLogoName);
        }else{
            relativeLayout.setVisibility(convertView.INVISIBLE);
            enterpriseNoLogo.setText("");
            Glide.with(context).load(enterprise.getPhoto()).override(105,80).into(enterpriseImage);
        }

        enterpriseName.setText(enterprise.getEnterpriseName());
        enterpriseBusiness.setText(enterprise.getBusiness());
        enterpriseLocation.setText(enterprise.getCountry());

        return convertView;
    }
}
