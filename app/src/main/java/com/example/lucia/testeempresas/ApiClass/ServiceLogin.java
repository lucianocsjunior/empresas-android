package com.example.lucia.testeempresas.ApiClass;

import com.example.lucia.testeempresas.HelpClass.AllEnterprises;
import com.example.lucia.testeempresas.Objects.loginUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Classe para recuperar o login e enviar os dados da Api
 * @author lucia
 * @version 1.0
 */

public interface ServiceLogin {
    @POST("/api/v1/users/auth/sign_in")
    @FormUrlEncoded
    Call<loginUser> userLogin(@Field("user") String user,@Field("pass") String pass);

    @GET("/api/v1/enterprises")
    Call<AllEnterprises> grantAcess(@Header("acess-token") String token, @Header("client") String client, @Header("uid")String uid);
}
